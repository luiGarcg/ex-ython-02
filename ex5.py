

class Veiculos(object):
    marca = None
    modelo = None
    preco = None
    tipo = None

    def comprar(self, modelo, tipo):
        if self.modelo == modelo and self.tipo == tipo:
            self.marca = None
            self.modelo = None
            self.preco = None
            self.tipo = None
        else:
            print("Veiculo não cadastrado")

    def cadastrar(self,marca, modelo, preco, tipo):
        if tipo == "carro" or tipo == "moto" or tipo == "caminhao":
            self.marca = marca
            self.modelo = modelo
            self.preco = preco
            self.tipo = tipo
        else:
            print("Tipo invalido")


    def listar(self):
        print(f'modelo: {self.modelo}')
        print(f'marca: {self.marca}')
        print(f'preço: R$ {self.preco}')
        print(f'Tipo: {self.tipo}')



veiculo = Veiculos()

while True:
    print("1. Comprar")
    print("2. Cadastrar")
    print("3. listar")
    print("4. sair")
    opcao = int(input("Digite a Oção desejada: "))

    if opcao == 1:
        print("Compre um veiculo")
        modelo = input("Digite o modelo: ")
        tipo = input("Digite o tipo: ")

        veiculo.comprar(modelo, tipo)



    elif opcao == 2:
        print("cadastre um veiculo")
        marca = input("Digite a marca: ")
        modelo = input("Digite o modelo: ")
        preco = float(input("Digite o preço: "))
        tipo = input("Digite o tipo: ")

        veiculo.cadastrar(marca,modelo,preco,tipo)

    elif opcao == 3:

        veiculo.listar()

    elif opcao == 4:
        break
    else:
        print("opção invalida")
