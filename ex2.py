class Banco:
    def __init__(self, numero_conta, nome):
        self.numero_conta = numero_conta
        self.nome = nome
        self.saldo = 0
    def alterar_nome(self, novoNome):
        self.nome = novoNome
    def depositar(self, valorDepositado):
        self.saldo += valorDepositado
    def sacar(self, valorRetirado):
        if(self.saldo <= 0 ):
            print("não é possivel realizar a operação")
        else:
            self.saldo -= valorRetirado

nome = input("Digite o nome da conta: ")
numero_conta = int(input("Digite o numero da conta: "))
conta = Banco(numero_conta, nome)
op = 0
while(op != 4):
    print("1 - Alterar seu nome")
    print("2 - Depositar")
    print("3 - Sacar")
    print("4 - sair")
    op = int(input("Digite uma opção: "))

    if(op == 1):
        novoNome = input("Digite o novo nome: ")
        conta.alterar_nome(novoNome)
    elif(op == 2):
        valorDepositado = int(input("Digite o valor para depositar: "))
        conta.depositar(valorDepositado)
    elif(op == 3):
        valorRetirado = int(input("Digite o valor para sacar: "))
        conta.sacar(valorRetirado)

    print(f'nome: {conta.nome}')
    print(f'numero da conta: {conta.numero_conta}')
    print(f'saldo: R${conta.saldo}')