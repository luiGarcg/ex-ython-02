# class Aluno(object):
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#         self.nota_1 = 0
#         self.nota_2 = 0
#     def addNotas(self, nota_1, nota_2):
#         self.nota_1 = nota_1
#         self.nota_2 = nota_2
#
#     def media(self):
#         return ((self.nota_1 + self.nota_2)/2)
#
#
#     def situacao(self):
#         media = self.media()
#
#         if(media >= 6):
#             situacao = "aprovado"
#         elif(media > 3 and media < 6):
#             situacao = "recuperacao"
#         else:
#             situacao = "reprovado"
#
#         return situacao
#
#
# #
# aluno_1 = Aluno("Luis", 18)
#
# aluno_2 = Aluno("marcio", 20)
# aluno_2.addNotas(2.0, 2.0)
# aluno_2.media()
# print(aluno_2.media(), aluno_2.situacao())


# print(dir(Aluno))

#herança

# class Animal(object):
#     pass
#
# class Ave(Animal):
#     pass
#
# class BeijaFlor(Ave):
#     pass

# class Pai(object):
#     nome = "Carlos"
#     sobrenome = "Oliveira"
#     residencia = "Santa Rita"
#     olhos = "Azuis"
# class Filha(Pai):
#     nome = "Luciana"
#     olhos = "Castanho"
#
# class Neta(Filha):
#     nome = "Maria"
#
# print(Pai.nome, Filha.nome, Neta.nome)
# print(Pai.residencia, Filha.residencia, Neta.residencia)
# print(Pai.olhos, Filha.olhos, Neta.olhos)

# class Pessoa(object):
#     nome = None
#     idade = None
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#
#     def envelhecer(self):
#         self.idade += 1
#
#
# class Atleta(Pessoa):
#     peso = None
#     aposentado = None
#
#     def __init__(self, nome, idade, peso):
#         super().__init__(nome,idade)
#         self.peso = peso
#
#     def aquecer(self):
#         print("Atleta aquecido!")
#     def aposentar(self):
#         self.aposentado = True
#
#
# class Corredor(Atleta):
#     def correr(self):
#         print("Correndo!")
#
# class Nadador(Atleta):
#     def nadar(self):
#         print("nadando!")
#
# class Ciclista(Atleta):
#     def pedalar(self):
#         print("Pedalando!")
#
# atleta_1 = Corredor("Luis", 18, 80)
# atleta_2 = Nadador("Marcelo", 21, 71)
# atleta_3 = Ciclista("Larissa", 19, 60)
#

# def decorador(func):
#     def wrapper():
#         print("Antes de executar")
#         func()
#         print("depois de executar")
#     return wrapper
#
# @decorador
# def media_turma():
#     nota_1 = 10
#     nota_2 = 7
#
#     print(f'Media: {(nota_1+nota_2)/2}')
#
# media_turma()

class CalculoImposto(object):
    def __init__(self):
        self.__taxa = 0

    @property
    def taxa(self):
        return round(self.__taxa, 2)
    @taxa.setter
    def taxa(self, val_taxa):
        if val_taxa < 0:
            self.__taxa = 0
        elif val_taxa > 100:
            self.__taxa = 100
        else:
            self.__taxa = val_taxa

calc = CalculoImposto()

calc.taxa = 55.25
print(calc.taxa)

calc.taxa = 105.60
print(calc.taxa)
